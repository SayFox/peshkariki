<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>Peshkariki</title>
		<link rel="stylesheet" href="/css/style.css" />
		<script type="text/javascript">
			function getIssuePoint() {
				var fio = document.getElementById('fio').value;
				var fioRegExp = /^[А-ЯЁ][а-яё]+(-[А-ЯЁ][а-яё]+)? [А-ЯЁ][а-яё]+( [А-ЯЁ][а-яё]+)?$/;
				var telephone = document.getElementById('telephone').value;
				var telephoneRegExp = /^[0-9]{10}$/;
				var adress = document.getElementById('adress').value;
				var response = document.getElementById('response');

				if (!fioRegExp.test(fio)) {
					alert('ФИО введено неверно!');

					return null;
				}
				if (!telephoneRegExp.test(telephone)) {
					alert('Телефон введен неверно!');

					return null;
				}
				if (!adress) {
					alert('Адрес не введен!');

					return null;
				}
				params = 'fio=' + encodeURIComponent(fio) +
					'&telephone=' + telephone +
					'&adress=' + encodeURIComponent(adress);
				var xhr = new XMLHttpRequest();
				xhr.open('GET', '/ajax.php?' + params, true);
				xhr.onreadystatechange = function() {
			     if(xhr.status == 200) {
					response.innerHTML = xhr.responseText;
				} else {alert('Ошибка сервера')}
			    }
			    xhr.setRequestHeader("X-Requested-With", "XMLHttpRequest");
				xhr.send(null);
			
				
			}
		</script>
	</head>
	<body>
		<div class="container">
			<table>
				<tr>
					<td><label>ФИО:</label></td>
					<td><input type="text" id="fio" required /></td>
				</tr>
				<tr>
					<td><label>Телефон (формат 9877045108):</label></td>
					<td><input type="text" id="telephone" required /></td>
				</tr>
				<tr>
					<td><label>Адрес:</label></td>
					<td><input type="text" id="adress" required /></td>
				</tr>
			</table>
				<input type="button" name="submit" value="Найти пункт выдачи" onclick="getIssuePoint()" />
		</div><br />
		<div class="container hidden" id='response'>
		</div>
	</body>
</html>
