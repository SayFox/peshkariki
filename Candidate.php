<?php
define('EARTH_RADIUS', 6372795);
require_once 'Toolkit.php';
require_once 'CandidateAbstract.php';

class Candidate extends CandidateAbstract {
	/**
     * ФИО
     *
     * @var string
     * @access public
     */
	public $fio;

	/**
     * Телефон
     *
     * @var int32
     * @access public
     */
	public $telephone;

	/**
     * Адресс
     *
     * @var string
     * @access public
     */
	public $adress;

	public function __construct() {
			$this->fio = $_GET['fio'];
			$this->telephone = $_GET['telephone'];
			$this->adress = $_GET['adress'];
	}

	/**
	  *
	  * @return string
	  */
	public function run() {
		
		$mysqli = new \mysqli(
            'localhost',  /* Хост, к которому мы подключаемся */
            'cm71718_sayfox',       /* Имя пользователя */
            '1234',   /* Используемый пароль */
            'cm71718_sayfox');     /* База данных для запросов по умолчанию */
            if ($mysqli->connect_errno) {
			    printf("Не удалось подключиться: %s\n", $mysqli->connect_error);
			    echo 'Не удалось получить доступ к базе данных точек доставки';
			    exit();
			}
		$link = $mysqli->query("SELECT * FROM `issuePoint`");
        if ($link->num_rows != 0) {
			$result = mysqli_fetch_all($link, MYSQLI_ASSOC);
			$coords = Toolkit::getCoords($this->adress);
	
			$minDist = EARTH_RADIUS;
			$issuePoint = '';
			foreach ($result as $entry) {
				$coordsCandidate = array_merge($entry, $coords);
				$dist = $this->calculateDistance($coordsCandidate);
				if ($minDist>$dist) {
					$minDist = $dist;
					$issuePoint = $entry['имя'];
				}
			}
			echo $this->fio . ' (' . Toolkit::getFormattedPhone($this->telephone) . '): ближайший пункт выдачи ' . $issuePoint . ' находится на расстоянии ' . $dist . 'км';
        } else {
        	echo 'В данный момент нет точек выдачи товаров';	
        }
    }

	/**
	  * Определение расстояния между координатами
	  *
     * @param float $lat1
     * @param float $long1
     * @param float $lat2
     * @param float $long2
     *
     * @return float
     */
	protected function calculateDistance($coordsCandidate) {
       
	   $lat1 = $coordsCandidate['широта'] * M_PI / 180;
	   $lat2 = $coordsCandidate['lat'] * M_PI / 180;
	   $long1 = $coordsCandidate['долгота'] * M_PI / 180;
	   $long2 = $coordsCandidate['lng'] * M_PI / 180;

	   $cl1 = cos($lat1);
	   $cl2 = cos($lat2);
	   $sl1 = sin($lat1);
	   $sl2 = sin($lat2);
	   $delta = $long2 - $long1;
	   $cdelta = cos($delta);
	   $sdelta = sin($delta);

	   $y = sqrt(pow($cl2 * $sdelta, 2) + pow($cl1 * $sl2 - $sl1 * $cl2 * $cdelta, 2));
	   $x = $sl1 * $sl2 + $cl1 * $cl2 * $cdelta;

	   $ad = atan2($y, $x);
	   $dist = $ad * EARTH_RADIUS;

	   return round($dist/1000, 1);
	}
}
